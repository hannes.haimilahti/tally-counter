git init
echo "node_modules" >> .gitignore
echo "logs" >> .gitignore
npm init -y 

npm install --save winston@3.12.0 express@4.18.2
npm pkg set 'type'="module"
npm pkg set 'scripts.start'="node src/main.mjs"
npm pkg set 'scripts.dev'="node --watch-path=src --watch src/main.mjs"
npm pkg set 'scripts.test'="mocha --timeout 5000"
npm install --save-dev mocha@10.3.0 chai@5.1.0

mkdir src test

touch rest.http
touch src/counter.mjs src/main.mjs src/logger.mjs src/router.mjs src/endpoint.mjs
touch test/counter.spec.mjs
