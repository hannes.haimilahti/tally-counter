import logger from "./logger.mjs";
import app from "./router.mjs";

const HOSTNAME = "127.0.0.1";
const PORT = 3000;

const main = () => {
    logger.log("info", "Service starting.");
    // Temporary error
    // logger.log("error", "This is a test error!");
    app.listen(PORT, HOSTNAME, () => {
        logger.log("info", `Listening at http://${HOSTNAME}:${PORT}`);
    });
};

main();
