import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mjs';

const app = express();
const counter = new Counter();

// Middleware
app.use(endpoint_mw);

// END POINTS
app.get('', (req, res) => {
    res.send("Welcome!");
});
app.get('/counter-increase', (req, res) => {
    counter.increase();
    res.send(`${counter.read()}`);
});
app.get('/counter-read', (req, res) => {
    res.send(`${counter.read()}`);
});
app.get('/counter-zero', (req, res) => {
    counter.zero();
    res.send(`${counter.read()}`);
});
app.all('*', (req, res) => {
    res.status(404).send("Resource not found!");
});

export default app;
